﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MängukooliMVC.Models;

namespace MängukooliMVC.Controllers
{
    public class OpetajaController : Controller
    {
        private ManguKoolEntities db = new ManguKoolEntities();

        // GET: Opetaja
        public ActionResult Index()
        {
            var inimesed = db.Inimesed
            .Where(x => x.Aine != null)
                .Include(i => i.MitaÕpetab).Include(i => i.Klass);
            return View(inimesed.ToList());
        }

        // GET: Opetaja/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inimene inimene = db.Inimesed.Find(id);
            if (inimene == null)
            {
                return HttpNotFound();
            }
            return View(inimene);
        }

        // GET: Opetaja/Create
        public ActionResult Create()
        {
            ViewBag.Aine = new SelectList(db.Ained, "AineKood", "Nimetus");
            ViewBag.KlassiKood = new SelectList(db.Klassid, "KlassiKood", "Nimetus");
            return View();
        }

        // POST: Opetaja/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Isikukood,Nimi,KlassiKood,Aine")] Inimene inimene)
        {
            if (ModelState.IsValid)
            {
                db.Inimesed.Add(inimene);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Aine = new SelectList(db.Ained, "AineKood", "Nimetus", inimene.Aine);
            ViewBag.KlassiKood = new SelectList(db.Klassid, "KlassiKood", "Nimetus", inimene.KlassiKood);
            return View(inimene);
        }

        // GET: Opetaja/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inimene inimene = db.Inimesed.Find(id);
            if (inimene == null)
            {
                return HttpNotFound();
            }
            ViewBag.Aine = new SelectList(db.Ained, "AineKood", "Nimetus", inimene.Aine);
            ViewBag.KlassiKood = new SelectList(db.Klassid, "KlassiKood", "Nimetus", inimene.KlassiKood);
            return View(inimene);
        }

        // POST: Opetaja/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Isikukood,Nimi,KlassiKood,Aine")] Inimene inimene)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inimene).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Aine = new SelectList(db.Ained, "AineKood", "Nimetus", inimene.Aine);
            ViewBag.KlassiKood = new SelectList(db.Klassid, "KlassiKood", "Nimetus", inimene.KlassiKood);
            return View(inimene);
        }

        // GET: Opetaja/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inimene inimene = db.Inimesed.Find(id);
            if (inimene == null)
            {
                return HttpNotFound();
            }
            return View(inimene);
        }

        // POST: Opetaja/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Inimene inimene = db.Inimesed.Find(id);
            db.Inimesed.Remove(inimene);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
