﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KoeraMVC.Models;

namespace KoeraMVC.Controllers
{
    public class InimeneController : Controller
    {
        // GET: Inimene
        public ActionResult Index()
        {
            return View(Inimene.Inimesed);
        }

        // GET: Inimene/Details/5
        public ActionResult Details(int id)
        {
            Inimene inimene = Inimene.Find(id);
                if (inimene == null) return HttpNotFound();
            return View(inimene);
        }

        // GET: Inimene/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inimene/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                new Inimene
                {
                    Kood = Inimene.NewKood(),
                    Nimi = collection["Nimi"],
                    Vanus = int.Parse(collection["Vanus"])
                }.Add();


                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }

        // GET: Inimene/Edit/5
        public ActionResult Edit(int id)
        {
            return View(Inimene.Find(id));
        }

        // POST: Inimene/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            Inimene inimene = Inimene.Find(id);
            if (inimene.Nimi == collection["Nimi"]
                && 
                inimene.Vanus == int.Parse(collection["Vanus"])
                )
                return RedirectToAction("Index");

            try
            {
                // TODO: Add update logic here

                inimene.Nimi = collection["Nimi"];
                inimene.Vanus = int.Parse(collection["Vanus"]);
                Inimene.Save();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Delete/5
        public ActionResult Delete(int id)
        {
            return View(Inimene.Find(id));
        }

        // POST: Inimene/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                Inimene.Find(id).Delete();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
