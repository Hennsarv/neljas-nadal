﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace KoeraCOnsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string connString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Henn\Data\Koerabaas.mdf;Integrated Security=True;Connect Timeout=30";

            // ehitan ja avan connectioni
            // selleks annan ette connection stringi
            SqlConnection conn = new SqlConnection(connString);
            conn.Open();
            string päring = "select * from inimene";

            // teen sql commandi (sql-päringu)
            // mis saadetakse (execute ajal) serverisse
            SqlCommand comm = new SqlCommand(päring, conn);

            // päringu vastus läheb muutujasse datareader
            var R = comm.ExecuteReader();
            while(R.Read())
            {
                Console.WriteLine(R["nimi"]);
            }

        }
    }
}
