﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EsimeneMVC.Models
{
    public class Inimene
    {
        // read list
        static Dictionary<int, Inimene> _Inimesed = new Dictionary<int, Inimene>();
        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;

        [Key]public int Kood { get; set; }
        public string Nimi { get; set; }
        public int Vanus { get; set; }

        // create
        public void Add()
        {
            if (!_Inimesed.Keys.Contains(this.Kood))
                _Inimesed.Add(this.Kood, this);
        }

        // read
        public static Inimene Find(int kood)
        =>
            //if (_Inimesed.Keys.Contains(kood)) return _Inimesed[kood];
            //else return null;
            _Inimesed.Keys.Contains(kood) ? _Inimesed[kood] : null;

        public static int NewKood()
            => _Inimesed.Keys.DefaultIfEmpty().Max() + 1;

        // delete
        public void Delete()
        {
            if (_Inimesed.Keys.Contains(this.Kood))
                _Inimesed.Remove(this.Kood);
        }

    }
}