﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EsimeneMVC.Controllers
{
    public class HomeController : Controller
    {
        public string Tere(string nimi) => $"Tere {nimi}!";

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Midat()
        {
            ViewBag.Message = "Henn tegi!";
            return View("Contact");
        }
    }
}