﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KoeraConsoleDE
{
    class Program
    {
        static void Main(string[] args)
        {
            KoerabaasEntities db = new KoerabaasEntities();
            foreach(var x in db.Inimesed)
                Console.WriteLine($"{x.Nimi} {x.Vanus}");

            Console.WriteLine($"vanuselt teine on {db.Inimesed.OrderByDescending(x => x.Vanus).Skip(1).FirstOrDefault()?.Nimi??"puudub" }");

            db.Inimesed
                .OrderByDescending(x => x.Vanus)
                .Skip(1)
                .FirstOrDefault().Nimi += " vana";

            db.SaveChanges();
        }
    }
}
