﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KoeraMVC.Models;

namespace KoeraMVC.Controllers
{
    public class KoerController : Controller
    {
        // GET: Koer
        public ActionResult Index()
        {
            // anname vastuseks view ja sellele parameetriks meie koerad
            return View(Koer.Koerad);
        }

        // GET: Koer/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Koer/Create
        public ActionResult Create()
        {
            ViewBag.InimeseKood 
                = new SelectList(Inimene.Inimesed, "Kood", "Nimi");
            return View();
        }

        // POST: Koer/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                new Koer
                {
                    Kood = Koer.NewKood(),
                    Nimi = collection["Nimi"],
                    Tõug = collection["Tõug"],
                    InimeseKood = int.Parse( collection["InimeseKood"])

                }.Add();

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }

        // GET: Koer/Edit/5
        public ActionResult Edit(int id)
        {
            Koer koer = Koer.Find(id);
            ViewBag.InimeseKood
               = new SelectList(Inimene.Inimesed, "Kood", "Nimi", koer.InimeseKood);
                // create ajal jääb viimane parameeter ära!
                // edit ajal - viimane parameeter näitab, kes on hetkel selle koera omanik

            return View(koer);
        }

        // POST: Koer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                Koer koer = Koer.Find(id);
                if (koer != null)
                {
                    koer.Nimi = collection["Nimi"];
                    koer.Tõug = collection["Tõug"];
                    koer.InimeseKood = int.Parse(collection["InimeseKood"]);

                }
                Koer.Save();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Koer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Koer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
