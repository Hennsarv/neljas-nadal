﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KoeradMaailmas2.Models;

namespace KoeradMaailmas2.Controllers
{
    public class InimenesController : Controller
    {
        private KoerabaasEntities db = new KoerabaasEntities();

        // GET: Inimenes
        public ActionResult Index()
        {
            return View(db.Inimesed.ToList());
        }

        // GET: Inimenes/Details/5 - henn kirjutab siia
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inimene inimene = db.Inimesed.Find(id);
            if (inimene == null)
            {
                return HttpNotFound();
            }
            return View(inimene);
        }

        // GET: Inimenes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inimenes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Kood,Nimi,Vanus")] Inimene inimene)
        {
            if (ModelState.IsValid)
            {
                db.Inimesed.Add(inimene);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(inimene);
        }

        // GET: Inimenes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inimene inimene = db.Inimesed.Find(id);
            if (inimene == null)
            {
                return HttpNotFound();
            }
            return View(inimene);
        }

        // POST: Inimenes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Kood,Nimi,Vanus")] Inimene inimene)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inimene).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inimene);
        }

        // GET: Inimenes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inimene inimene = db.Inimesed.Find(id);
            if (inimene == null)
            {
                return HttpNotFound();
            }
            return View(inimene);
        }

        // POST: Inimenes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Inimene inimene = db.Inimesed.Find(id);
            db.Inimesed.Remove(inimene);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
