﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MängukooliMVC.Models;



namespace MängukooliMVC.Controllers
{
    public class InimeneController : Controller
    {
        private ManguKoolEntities db = new ManguKoolEntities();

        // GET: Inimene - kõik inimesed
        public ActionResult Index()
        {
            var inimesed = db.Inimesed
                
                .Include(i => i.MitaÕpetab).Include(i => i.Klass);
            return View(inimesed.ToList());
        }

        public ActionResult Opilased()
        {
            var inimesed = db.Inimesed
                .Where(x => x.MitaÕpetab == null && x.Klass != null)
                .Include(i => i.MitaÕpetab).Include(i => i.Klass);
            return View("Index",inimesed.ToList());
        }

        public ActionResult Opetajad()
        {
            var inimesed = db.Inimesed
                .Where(x => x.Aine != null)
                .Include(i => i.MitaÕpetab).Include(i => i.Klass);
            return View("Index", inimesed.ToList());
        }

        public ActionResult Lapsevanemad()
        {
            var inimesed = db.Inimesed.Include(i => i.MitaÕpetab).Include(i => i.Klass);
            return View("Index", inimesed.ToList());
        }



        // GET: Inimene/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inimene inimene = db.Inimesed.Find(id);
            if (inimene == null)
            {
                return HttpNotFound();
            }

            bool KasÕpilane = inimene.Aine == null && inimene.Klass != null;
            bool KasÕpetaja = inimene.Aine != null;

            ViewBag.Kes = KasÕpilane ? "Õpilane" : KasÕpetaja ? "Õpetaja" : "Lapsevanem";

            ViewBag.Klassid = db.Klassid.ToList();

            return View(inimene);
        }

        // GET: Inimene/Create
        public ActionResult Create()
        {

            //Aine puuduAine = new Aine(); // { Nimetus = "", AineKood = "" };
            //List<Aine> ained = db.Ained.ToList();
            //ained.Add(puuduAine);

            //ViewBag.Aine = new SelectList(ained.OrderBy(x => x.AineKood), "AineKood", "Nimetus");
            ViewBag.Aine = new SelectList(db.Ained.AsEnumerable().FirstEmpty(), "AineKood", "Nimetus");

            //  Klass puuduKlass = new Klass { Nimetus = "", KlassiKood = "" };
            //  List<Klass> klassid = db.Klassid.ToList();
            //  klassid.Add(puuduKlass);

            //var klassid = (new List<Klass> { new Klass() }).Union(db.Klassid);

            //List<Klass> klassid = new List<Klass> { new Klass() };
            //klassid.AddRange(db.Klassid);

            ViewBag.KlassiKood = new SelectList((new List<Klass> { new Klass { Nimetus =".. vali klass.." } }).Union(db.Klassid), "KlassiKood", "Nimetus");
            return View();
        }

        // POST: Inimene/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Isikukood,Nimi,KlassiKood,Aine")] Inimene inimene)
        {
            if (ModelState.IsValid)
            {
                if (inimene.KlassiKood == "") inimene.KlassiKood = null;
                if (inimene.Aine == "") inimene.Aine = null;

                db.Inimesed.Add(inimene);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Aine = new SelectList(db.Ained, "AineKood", "Nimetus", inimene.Aine);
            ViewBag.KlassiKood = new SelectList(db.Klassid, "KlassiKood", "Nimetus", inimene.KlassiKood);
            return View(inimene);
        }

        // GET: Inimene/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inimene inimene = db.Inimesed.Find(id);
            if (inimene == null)
            {
                return HttpNotFound();
            }
            ViewBag.Aine = new SelectList(db.Ained, "AineKood", "Nimetus", inimene.Aine);
            ViewBag.KlassiKood = new SelectList(db.Klassid.FirstEmpty(), "KlassiKood", "Nimetus", inimene.KlassiKood);
            return View(inimene);
        }

        // POST: Inimene/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Isikukood,Nimi,KlassiKood,Aine")] Inimene inimene)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inimene).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Aine = new SelectList(db.Ained, "AineKood", "Nimetus", inimene.Aine);
            ViewBag.KlassiKood = new SelectList(db.Klassid, "KlassiKood", "Nimetus", inimene.KlassiKood);
            return View(inimene);
        }

        // GET: Inimene/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inimene inimene = db.Inimesed.Find(id);
            if (inimene == null)
            {
                return HttpNotFound();
            }
            return View(inimene);
        }

        // POST: Inimene/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Inimene inimene = db.Inimesed.Find(id);
            db.Inimesed.Remove(inimene);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
