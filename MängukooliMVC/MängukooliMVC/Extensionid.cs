﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MängukooliMVC
{
    static public class Extensionid
    {
        public static IEnumerable<T> FirstEmpty<T>(this IEnumerable<T> coll)  
            where T : new()
        {
            yield return new T();
            foreach (var x in coll) yield return x;
        }
        public static IEnumerable<T> FirstEmpty<T>(this IQueryable<T> coll)
            where T : new()
        {
            yield return new T();
            foreach (var x in coll) yield return x;
        }
        public static IEnumerable<T> FirstNull<T>(this IEnumerable<T> coll) 
            where T : struct
        {
            T[] t = new T[1];
            yield return t[0];
            foreach (var x in coll) yield return x;


        }
    }
}