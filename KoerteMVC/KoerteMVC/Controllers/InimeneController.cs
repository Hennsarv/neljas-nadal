﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KoerteMVC.Models;

namespace KoerteMVC.Controllers
{
    public class InimeneController : Controller
    {
        private KoerabaasEntities db = new KoerabaasEntities();

        // GET: Inimene
        public ActionResult Index()
        {
            return View(db.Inimesed.ToList());
        }

        // GET: Inimene/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inimene inimene = db.Inimesed.Find(id);
            if (inimene == null)
            {
                return HttpNotFound();
            }
            return View(inimene);
        }

        // GET: Inimene/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inimene/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Kood,Nimi,Vanus")] Inimene inimene)
        {
            if (ModelState.IsValid)
            {
                db.Inimesed.Add(inimene);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(inimene);
        }

        // GET: Inimene/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inimene inimene = db.Inimesed.Find(id);
            if (inimene == null)
            {
                return HttpNotFound();
            }
            return View(inimene);
        }

        // POST: Inimene/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Kood,Nimi,Vanus")] Inimene inimene)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inimene).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inimene);
        }

        // GET: Inimene/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inimene inimene = db.Inimesed.Find(id);
            if (inimene == null)
            {
                return HttpNotFound();
            }
            return View(inimene);
        }

        // POST: Inimene/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Inimene inimene = db.Inimesed.Find(id);
            db.Inimesed.Remove(inimene);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
