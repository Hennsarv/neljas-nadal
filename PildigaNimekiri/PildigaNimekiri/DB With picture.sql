﻿

create table Picture
(
	Id int identity primary key,
	Content varbinary(max) null
)
create table Person
(
	Id int identity primary key,
	Name nvarchar(32) not null,
	PictureId int null references Picture(Id)
)
