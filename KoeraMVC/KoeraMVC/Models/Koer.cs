﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;

namespace KoeraMVC.Models
{
    public class Koer
    {
        static string filename = @"c:\henn\data\koerad.json";
        // esimene on koht, kus me koeri hoiame
        // teine on property nende kuvamiseks-näitamiseks
        static Dictionary<int, Koer> _Koerad; // = new Dictionary<int, Koer>();
        public static IEnumerable<Koer> Koerad => _Koerad.Values;

        static Koer()
        {
            try
            {
                _Koerad = JsonConvert.DeserializeObject<Dictionary<int, Koer>>(File.ReadAllText(filename));
            }
            catch
            {
                _Koerad = new Dictionary<int, Koer>();
            }

        }

        // need on siis väljad-propertid, milles Koer koosneb
        [Key] // see väli on koera kood - unikaalne number
        public int Kood { get; set; }

        
        [Display(Name = "Koera nimi")] // neid kahte vaatame hiljem
        public string Nimi { get; set; }

        [Display(Name = "Koera tõug")] // viewdes kuvamisel kasutame pikemat nimetust
        public string Tõug { get; set; }

        //[MaxLength(20)] // omaniku nimi ei tohi olla pikem kui 20 tähemärki
        //public string Omanik { get; set; }

        [Display(Name = "Omanik (kood)")]
        public int InimeseKood { get; set; }

        [JsonIgnore]
        [Association("Inimene", "InimeseKood", "Kood")]
        public Inimene Omanik => Inimene.Find(InimeseKood);


        // data annotatsioonid on klassi kirjelduses täiendavad välja (property) tunnused
        // mis jutustavad, kuias antud välja kasutada
        // annotatsioon kirjutatakse välja ette (enamasti eelmisele reale)
        // nurksulgudesse


        // Koera leidmine koodi järgi
        public static Koer Find(int kood)
            => _Koerad.Keys.Contains(kood) ? _Koerad[kood] : null;

        // Koera lisamine nimekirja
        public void Add()
        {
            if (!_Koerad.Keys.Contains(this.Kood))
            {
                _Koerad.Add(this.Kood, this);
                Save();
            }
        }

        // koera eemaldamine nimekirjast
        public void Remove()
        {
            if (_Koerad.Keys.Contains(this.Kood))
            {
                _Koerad.Remove(this.Kood);
                Save();
            }
        }

        // selle funktsiooniga saab küsida numbreid, mida koertel veel pole
        public static int NewKood()
            => _Koerad.Keys.DefaultIfEmpty().Max() + 1;

        public static void Save()
        {
           File.WriteAllText(filename, JsonConvert.SerializeObject(_Koerad ));

        }
    }
}