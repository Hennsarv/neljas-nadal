﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KoeradMaailmas2.Models;

namespace KoeradMaailmas2.Controllers
{
    public class KoersController : Controller
    {
        private KoerabaasEntities db = new KoerabaasEntities();

        // GET: Koers
        public ActionResult Index()
        {
            var koerad = db.Koerad.Include(k => k.Inimene);
            return View(koerad.ToList());
        }

        // GET: Koers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Koer koer = db.Koerad.Find(id);
            if (koer == null)
            {
                return HttpNotFound();
            }
            return View(koer);
        }

        // GET: Koers/Create
        public ActionResult Create()
        {
            ViewBag.InimeseKood = new SelectList(db.Inimesed, "Kood", "Nimi");
            return View();
        }

        // POST: Koers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Kood,Nimi,Tõug,InimeseKood")] Koer koer)
        {
            if (ModelState.IsValid)
            {
                db.Koerad.Add(koer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.InimeseKood = new SelectList(db.Inimesed, "Kood", "Nimi", koer.InimeseKood);
            return View(koer);
        }

        // GET: Koers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Koer koer = db.Koerad.Find(id);
            if (koer == null)
            {
                return HttpNotFound();
            }
            ViewBag.InimeseKood = new SelectList(db.Inimesed, "Kood", "Nimi", koer.InimeseKood);
            return View(koer);
        }

        // POST: Koers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Kood,Nimi,Tõug,InimeseKood")] Koer koer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(koer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.InimeseKood = new SelectList(db.Inimesed, "Kood", "Nimi", koer.InimeseKood);
            return View(koer);
        }

        // GET: Koers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Koer koer = db.Koerad.Find(id);
            if (koer == null)
            {
                return HttpNotFound();
            }
            return View(koer);
        }

        // POST: Koers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Koer koer = db.Koerad.Find(id);
            db.Koerad.Remove(koer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
