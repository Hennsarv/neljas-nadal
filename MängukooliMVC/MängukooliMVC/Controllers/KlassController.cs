﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MängukooliMVC.Models;

namespace MängukooliMVC.Controllers
{
    public class KlassController : Controller
    {
        private ManguKoolEntities db = new ManguKoolEntities();

        // GET: Klass
        public ActionResult Index()
        {
            return View(db.Klassid.ToList());
        }

        // GET: Klass/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Klass klass = db.Klassid.Find(id);
            if (klass == null)
            {
                return HttpNotFound();
            }

            // teeme kaks loetelu (listi) ja paneme need ViewBagi kaasa
            // siis saab neid views kasutada
            // üks sisaldab Aineid, mida selles klassis õpetatakse
            // teine Aineid, mida ei õpetata
            var klassiAined = klass.Ained.Select(x => x.AineKood).ToList();
            ViewBag.Ained = db.Ained.Where(x => klassiAined.Contains(x.AineKood)).ToList();
            ViewBag.EiAined = db.Ained.Where(x => !klassiAined.Contains(x.AineKood)).ToList();
            return View(klass);
        }

        // GET: Klass/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Klass/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KlassiKood,Nimetus")] Klass klass)
        {
            if (ModelState.IsValid)
            {
                db.Klassid.Add(klass);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(klass);
        }

        // GET: Klass/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Klass klass = db.Klassid.Find(id);
            if (klass == null)
            {
                return HttpNotFound();
            }
            return View(klass);
        }

        // POST: Klass/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KlassiKood,Nimetus")] Klass klass)
        {
            if (ModelState.IsValid)
            {
                db.Entry(klass).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(klass);
        }

        // GET: Klass/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Klass klass = db.Klassid.Find(id);
            if (klass == null)
            {
                return HttpNotFound();
            }
            return View(klass);
        }

        // POST: Klass/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Klass klass = db.Klassid.Find(id);
            db.Klassid.Remove(klass);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // siit algab minu lisatud osa

        // uus meetod, mis tehtud aine lisamiseks klassile
        // klassi "kood" on hea anda parameetriga "id" - siis kasutatakse
        // ruutingu loogikat (Klass/LisaAine/1B?aine=esta) 
        public ActionResult LisaAine(string id, string aine)
        {
            Aine a = db.Ained.Find(aine);   // leiame parameetri järgi
            Klass k = db.Klassid.Find(id);  // aine ja klassi 
            if (a != null && k != null)     // kui need on olemas
            {
                AineKlass ak = new AineKlass { Aine = a, Klass = k };
                k.Ained.Add(ak);        // teeme uue objekti 
                db.SaveChanges();       // ja lisame selle baasi
            }
            // kui siis tehtud, suuname tagasi Actionile Details
            return RedirectToAction("Details", new { id });
        }

        // teine meetod siis vastupidiseks toiminguks
        public ActionResult EemaldaAine(string id, string aine)
        {
            Aine a = db.Ained.Find(aine);
            Klass k = db.Klassid.Find(id);
            if (a != null && k != null)
            {
                db.AineKlassid.Remove(
                a.Klassid.Where(x => x.Klass == k).SingleOrDefault());
                db.SaveChanges();
            }
            return RedirectToAction("Details", new { id });
        }
    }
}
