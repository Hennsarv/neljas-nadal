﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;

namespace KoeraMVC.Models
{
    public class Inimene
    {
        static string filename = @"c:\Henn\Data\Inimesed.json";
        // read list
        static Dictionary<int, Inimene> _Inimesed; // = new Dictionary<int, Inimene>();
        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;

        static Inimene()
        {
            try
            {
                _Inimesed = JsonConvert.DeserializeObject<Dictionary<int, Inimene>>(File.ReadAllText(filename));
            }
            catch
            {
                _Inimesed = new Dictionary<int, Inimene>();
            }
        }

        [Key] public int Kood { get; set; }
        public string Nimi { get; set; }
        public int Vanus { get; set; }

        [JsonIgnore]
        public ICollection<Koer> Koerad => Koer.Koerad.Where(x => x.InimeseKood == Kood).ToList();

        // create
        public void Add()
        {
            if (!_Inimesed.Keys.Contains(this.Kood))
            {
                _Inimesed.Add(this.Kood, this);
                Save();
            }
        }

        // read
        public static Inimene Find(int kood)
        =>
            //if (_Inimesed.Keys.Contains(kood)) return _Inimesed[kood];
            //else return null;
            _Inimesed.Keys.Contains(kood) ? _Inimesed[kood] : null;

        public static int NewKood()
            => _Inimesed.Keys.DefaultIfEmpty().Max() + 1;

        // delete
        public void Delete()
        {
            if (_Inimesed.Keys.Contains(this.Kood))
            {
                _Inimesed.Remove(this.Kood);
                Save();
            }
        }

        static public void Save()
        { 
            
            File.WriteAllText(filename, JsonConvert.SerializeObject(_Inimesed));
        }
    }
}